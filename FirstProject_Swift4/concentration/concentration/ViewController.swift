//
//  ViewController.swift
//  concentration
//
//  Created by Patrick Sturgeon on 12/11/18.
//  Copyright © 2018 Patrick Sturgeon. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    lazy var Game = Concentration(numberOfPairsOfCards: (CardButtons.count + 1) / 2)
    var flipCount = 0
    {didSet  {FlipCountLabel.text = "Flips: \(flipCount)"}}
    @IBOutlet weak var FlipCountLabel: UILabel!
    @IBOutlet var CardButtons: [UIButton]!
    var emojiChoices = ["👻","🎃","🕷","👺","🍬"]
    var emoji = [Int:String]()
    @IBAction func TouchCard(_ sender: UIButton)
    {
        flipCount += 1
        if let cardNumber = CardButtons.index(of: sender)
        {
            Game.ChooseCard(at: cardNumber)
            updateViewFromModel()
        }
    }
    func updateViewFromModel()
    {
        for index in CardButtons.indices
        {
            let button = CardButtons[index]
            let card = Game.cards[index]
            if card.isFaceUp
            {
                button.setTitle(Emoji(for: card), for: UIControlState.normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else
            {
                button.setTitle("", for: UIControlState.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }
    }
    func Emoji(for card: Card) -> String
    {
        if emoji[card.indentifier] == nil, emojiChoices.count > 0
            {
                let randomIndex = Int(arc4random_uniform(UInt32(emojiChoices.count)))
                emoji[card.indentifier] = emojiChoices.remove(at: randomIndex)
            }
        return emoji[card.indentifier] ?? "?"
        //if emoji[card.indentifier] != nil
        //{
        //    return emoji[card.indentifier]!
        //}
        //else
        //{
        //    return "?"
        //}
    }
    
    //TODO: Main Menu, End Menu, New Game Button
}

