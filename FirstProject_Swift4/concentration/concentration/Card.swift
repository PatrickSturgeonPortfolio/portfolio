//
//  Card.swift
//  concentration
//
//  Created by Patrick Sturgeon on 12/11/18.
//  Copyright © 2018 Patrick Sturgeon. All rights reserved.
//

import Foundation

struct Card
{
    var isFaceUp = false
    var isMatched = false
    var indentifier: Int
    
    static var identifierFactory = 0
    
    static func GetUniqueIdentifier() -> Int
    {
        identifierFactory += 1
        return identifierFactory
    }
    
    init()
    {
        self.indentifier = Card.GetUniqueIdentifier()
    }
}
