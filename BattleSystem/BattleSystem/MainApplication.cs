﻿using System;

namespace BattleSystem
{
    public class MainApplication
    {
        //string playerName = "Patrick";
        Enemy enemy = new Enemy();
        Player player = new Player();
        Random random = new Random();
        string r = ""; //used to collect user input.

        public MainApplication()
        { 
            BattleDisplay();
            Console.ReadKey();
        }

        public void Selection()
        {
            r = Utility.ValidateString("\n\nPlease make a choice");
            switch (r)
            {
                case "1":
                case "attack":
                    enemy.CurrHealth = enemy.CurrHealth - (player.Attack + random.Next(0,4));
                    player.CurrHealth = player.CurrHealth - (enemy.Attack + random.Next(0,4));
                    if (enemy.CurrHealth <= 0)
                    {
                        Console.Clear();
                        Console.WriteLine("Player Wins");
                        Console.ReadKey();
                        }else if (player.CurrHealth <= 0)
                    {
                        Console.Clear();
                        Console.WriteLine("Computer wins");
                        Console.ReadKey();
                    }
                    BattleDisplay();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("That was an invalid selection!\n\nPress any key to return...");
                    Console.ReadKey();
                    BattleDisplay();
                    break;
            }
        }

        public void BattleDisplay()
        {
            Console.Clear();
            Console.WriteLine(enemy.Name);
            Console.WriteLine(enemy.Level);
            string t = HealthCal(enemy.MaxHealth,enemy.CurrHealth);
            Console.WriteLine($"Health: {t}");
            Console.WriteLine($"\n\n{player.Name}");
            Console.WriteLine(player.Level);
            t = HealthCal(player.MaxHealth, player.CurrHealth);
            Console.WriteLine($"Health: {t}");
            Console.WriteLine("\n\n1. Attack");
            Selection();
        }
        public string HealthCal(int max, int curr)
        {
            Console.WriteLine(curr);
            //Console.WriteLine(max);
            double temp = ((double)curr / (double)max);
            if (temp >= 1)
            {
                return "|||||||||||";
            }else if (temp < 1 && temp >= .9)
            {
                return "||||||||||";
            }else if (temp >= .8 && temp <= .89)
            {
                return "|||||||||";
            }else if (temp >= .7 && temp <= .79)
            {
                return "||||||||";
            }else if (temp >= .6 && temp <= .69)
            {
                return "|||||||";
            }else if (temp >= .5 && temp <= .69)
            {
                return "||||||";
            }else if (temp >= .4 && temp <= .59)
            {
                return "|||||";
            }else if (temp >= .3 && temp <= .39)
            {
                return "||||";
            }else if (temp >= .2 && temp <= 2.9)
            {
                return "|||";
            }else if (temp >= .1 && temp <= .19)
            {
                return "||";
            }else if (temp >= .01 && temp <= .09)
            {
                return "|";
            }else
            {
                return "";
            }
        }
    }
}
