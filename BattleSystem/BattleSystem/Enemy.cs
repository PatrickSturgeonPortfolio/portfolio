﻿using System;
namespace BattleSystem
{
    public class Enemy
    {
        Random random = new Random();

        public string Name { get; set; }
        public int MaxHealth { get; set; }
        public int CurrHealth { get; set; }
        public int Level { get; set; }
        public int Attack { get; set; }

        public Enemy()
        {
            Name = "Ogre";
            MaxHealth = 100;
            CurrHealth = MaxHealth;
            Level = 2;
            Attack = random.Next(5, 8);
        }
    }
}
