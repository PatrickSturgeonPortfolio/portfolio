﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleSystem
{
    class Utility
    {
        public static float GetAverage(int[] numbers)
        {
            int total = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                total += numbers[i];
            }
            // float avg = total / numbers.Length;
            return total / numbers.Length;
        }
        public static int ValidateInt(string prompt)
        {
            int result;
            Console.Write($"{prompt}: ");
            string response = Console.ReadLine();
            while (!int.TryParse(response, out result))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Enter an integer only");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"{prompt}: ");
                response = Console.ReadLine();
            }
            return result;
        }
        public static string ValidateString(string prompt)
        {
            Console.Write($"{prompt}: ");
            string response = Console.ReadLine();

            while (String.IsNullOrWhiteSpace(response))
            {
                Console.WriteLine("Please do not leave this blank.");
                response = Console.ReadLine();
            }
            return response;
        }
        public static decimal ValidateDecimal(string prompt)
        {
            decimal result;
            Console.Write($"{prompt}: ");
            string response = Console.ReadLine();
            while (!decimal.TryParse(response, out result))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Enter an decimal only");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"{prompt}: ");
                response = Console.ReadLine();
            }
            return result;
        }

        public static float ValidateFloat(string prompt)
        {
            float result;
            Console.WriteLine($"{prompt}");
            string response = Console.ReadLine();
            while (!float.TryParse(response, out result))
            {
                Console.WriteLine("Enter a float value only");
                Console.WriteLine($"{prompt}");
                response = Console.ReadLine();
            }
            return result;
        }
    }
}
