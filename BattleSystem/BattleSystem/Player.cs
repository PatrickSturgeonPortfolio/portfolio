﻿using System;
namespace BattleSystem
{
    public class Player
    {
        Random random = new Random();

        public string Name { get; set; }
        public int MaxHealth { get; set; }
        public int CurrHealth { get; set; }
        public int Level { get; set; }
        public int Attack { get; set; }

        public Player()
        {
            Name = "Patrick";
            MaxHealth = 100;
            CurrHealth = MaxHealth;
            Level = 3;
            Attack = random.Next(5, 10);
        }
    }
}
